#! /usr/bin/env Rscript

require (stats);

summary (longley);

svg (filename = "pairs.svg");
pairs (longley);
dev.off ();

longley.y <- longley[, "GNP"];
longley.x <- longley[,c(3:7)];

fm1 <- lm (GNP ~ Armed.Forces + Population + Employed, data = longley)

summary (fm1);

svg (filename = "model-plots.svg");
opar <- par (mfrow = c (2, 2), oma = c (0, 0, 1.1, 0),
             mar = c (4.1, 4.1, 2.1, 1.1));
plot (fm1);
par (opar);
dev.off ();
