#! /usr/bin/env Rscript

rovnomerne = scan("rovnomerne.data")
normalni = scan("normalni.data")

#png(filename = "qq-graph-comparison.png")
pdf(file = "qq-graph-comparison.pdf", encoding="CP1250")
layout(matrix(c(1, 2), 1, 2, byrow = TRUE), widths = c(1,1), respect=TRUE)
# par(mfcol=c(1,2))
qqnorm(rovnomerne, main = "Rovnoměrné rozdělení", xlab = "Teoretické kvantily", ylab = "Vzorkové kvantily")
qqnorm(normalni, main = "Normální rozdělení", xlab = "Teoretické kvantily", ylab = "Vzorkové kvantily")
dev.off()
